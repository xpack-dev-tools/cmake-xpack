# The xPack CMake

**The xPack CMake** is the **xPack** version of
the **CMake** build system, an open source project.

For more details, please read the corresponding release pages:

- <https://xpack.github.io/cmake/releases/>
- <http://cmake.org>

Thank you for using open source software,

Liviu Ionescu
