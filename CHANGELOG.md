# Change & release log

Entries in this file are in reverse chronological order.

## 2022-09-01

- v3.22.6-1.1 published on npmjs.com
- v3.22.6-1 released

## 2022-03-25

- v3.21.6-1.1 published on npmjs.com
- v3.21.6-1 released

## 2021-12-06

- v3.20.6-2 released

## 2021-11-20

- v3.20.6-2 prepared
- add support for Apple Silicon

## 2021-10-18

- v3.20.6-1.1 published on npmjs.com
- v3.20.6-1 released
- v3.20.6-1 prepared

## 2021-09-11

- update for the new build scripts

## 2021-05-19

- v3.19.8-1.1 published on npmjs.com
- v3.19.8-1 released

## 2021-03-17

- v3.19.2-2.1 published on npmjs.com
- v3.19.2-2 prepared

## 2021-03-16

- v3.18.6-1.1 published on npmjs.com
- v3.18.6-1 released
- add cmd.exe support

## 2021-01-06

- v3.19.2-1.1 published on npmjs.com
- v3.19.2-1 released

## 2020-12-15

- v3.19.1-1.1 published on npmjs.com
- v3.19.1-1 released

## 2020-12-09

- v3.18.5-1.1 published on npmjs.com
- v3.18.5-1 released

## 2020-09-29

- v3.18.3-1.1 published on npmjs.com
- v3.18.3-1 released

## 2020-07-22

- prepare v3.17.3-1
